#!/usr/bin/env bash

RELEASE="2.3.1"
OCSPACKAGE="OCSNG_UNIX_SERVER-$RELEASE.tar.gz"

export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get upgrade -y
apt-get dist-upgrade -y
apt-get install -y \
        apache2 \
        mysql-server \
        libapache2-mod-php5 \
        libapache2-mod-perl2 \
        php5-mysql \
        php5-gd \
        php-pclzip \
        libdbd-mysql-perl \
        libxml-simple-perl \
        libapache-dbi-perl \
        libnet-ip-perl \
        libsoap-lite-perl \
        libarchive-zip-perl \
        make
if [ -f $OCSPACKAGE ]; then
  echo "Package $OCSPACKAGE already downloaded"
else
  wget "https://github.com/OCSInventory-NG/OCSInventory-ocsreports/releases/download/$RELEASE/$OCSPACKAGE"
  tar xzf $OCSPACKAGE
fi
