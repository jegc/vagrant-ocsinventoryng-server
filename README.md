# Prepare Vagrant environment

Execute the nexto command to create a Vagrant file that use Ubuntu Xenial box

```shell
vagrant init debian/jessie64
```

# Write a provision script

Create a script named `bootstrap.sh` with the next content

```shell
#!/usr/bin/env bash

RELEASE="2.3.1"
OCSPACKAGE="OCSNG_UNIX_SERVER-$RELEASE.tar.gz"

export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get upgrade -y
apt-get dist-upgrade -y
apt-get install -y \
        apache2 \
        mysql-server \
        libapache2-mod-php5 \
        libapache2-mod-perl2 \
        php5-mysql \
        php5-gd \
        php-pclzip \
        libdbd-mysql-perl \
        libxml-simple-perl \
        libapache-dbi-perl \
        libnet-ip-perl \
        libsoap-lite-perl \
        libarchive-zip-perl \
        make
if [ -f $OCSPACKAGE ]; then
  echo "Package $OCSPACKAGE already downloaded"
else
  wget "https://github.com/OCSInventory-NG/OCSInventory-ocsreports/releases/download/$RELEASE/$OCSPACKAGE"
  tar xzf $OCSPACKAGE
fi
```

Configure Vagrant to run the script when setting up the machine adding the next line to the Vagrant file before the last `end` word.

```ruby
config.vm.provider "virtualbox" do |vb|
    vb.memory = "4096"
    vb.cpus = "2"
end
config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
config.vm.provision "shell", path: "bootstrap.sh"
```

# Write tests

Create a Gemfile with serverspec

```ruby
source 'https://rubygems.org'

gem 'serverspec'
gem 'rake'
```

Download the requiered gems with bundle:

```shell
bundle install --path vendor/bundle
```

Init the serverspec environment:

````shell
bundle exec serverspec-init
elect OS type:

  1) UN*X
  2) Windows

Select number: 1

Select a backend type:

  1) SSH
  2) Exec (local)

Select number: 1

Vagrant instance y/n: y
Auto-configure Vagrant from Vagrantfile? y/n: y
 + spec/
 + spec/default/
 + spec/default/sample_spec.rb
 + spec/spec_helper.rb
 + Rakefile
 + .rspec
```

Remove `spec/default/sample_spec.rb` and create some tests.

Apache tests `spec/default/apache_spec.rb`:

````ruby
re 'spec_helper'

describe package('apache2') do
  it { should be_installed }
end

describe service('apache2') do
  it { should be_enabled }
  it { should be_running }
end

describe port(80) do
  it { should be_listening }
end
```

Dependencies tests `spec/defaul/dependencies_spec.rb`:

```ruby
require 'spec_helper'

packages = ['apache2',
            'mysql-server',
            'libapache2-mod-php5',
            'libapache2-mod-perl2',
            'php5-mysql',                                                                                            
            'php5-gd',                                                                                               
            'php-pclzip',                                                                                            
            'libxml-simple-perl',
            'libapache-dbi-perl',
            'libnet-ip-perl',
            'libsoap-lite-perl',
            'libarchive-zip-perl',
            'make']

describe 'Test dependencies' do
  packages.each do |pkg|
    describe package("#{pkg}") do
      it { should be_installed }
    end
  end
end
```

# Create vm and run tests

Vagrant

```shell
vagrant up
```

Serverspec

```shell
bundle exec rake
```

If at least a test fail, fix provision scrpit to satisfy all failed tests.


# Install OCS Inventory NG Server

Login in to the virtual machine

````shell
vagrant ssh

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
vagrant@jessie:~$
```

Run ```setup.sh``` script located in to ```OCSNG_UNIX_SERVER-2.3.1``` and for the first question just type ```ENTER```

```shell
cd OCSNG_UNIX_SERVER-2.3.1
sudo ./setup.sh 

+----------------------------------------------------------+
|                                                          |
|  Welcome to OCS Inventory NG Management server setup !   |
|                                                          |
+----------------------------------------------------------+

Trying to determine whitch OS or Linux distribution you use
+----------------------------------------------------------+
| Checking for Apache web server binaries !		   |
+----------------------------------------------------------+

CAUTION: If upgrading Communication server from OCS Inventory NG 1.0 RC2 and
previous, please remove any Apache configuration for Communication Server!

Do you wish to continue ([y]/n)?
Assuming Communication server 1.0 RC2 or previous is not installed
on this computer.

Starting OCS Inventory NG Management server setup from folder /home/vagrant/OCSNG_UNIX_SERVER-2.3.1
Storing log in file /home/vagrant/OCSNG_UNIX_SERVER-2.3.1/ocs_server_setup.log
```

## MySQL

For the next question if your database is located in other server type the hostname or IP address, otherwise jost type ```ENTER```

```shell
+----------------------------------------------------------+
| Checking for database server properties...	           |
+----------------------------------------------------------+

Your MySQL client seems to be part of MySQL version 5.5.
Your computer seems to be running MySQL 4.1 or higher, good ;-)

Which host is running database server [localhost] ?
OK, database server is running on host localhost ;-)
```

If your database is running in the standard port 3306 type the number of that other port, otherwise jost type ```ENTER```

````shell
On which port is running database server [3306] ?
OK, database server is running on port 3306 ;-)
```

## Apache

```shell
+----------------------------------------------------------+
| Checking for Apache web server daemon...		   |
+----------------------------------------------------------+

Where is Apache daemon binary [/usr/sbin/apache2ctl] ?
OK, using Apache daemon /usr/sbin/apache2ctl ;-)
```

````shell
+----------------------------------------------------------+
| Checking for Apache main configuration file...           |
+----------------------------------------------------------+

Where is Apache main configuration file [/etc/apache2/apache2.conf] ?
OK, using Apache main configuration file /etc/apache2/apache2.conf ;-)
```

```shell
+----------------------------------------------------------+
| Checking for Apache user account...			   |
+----------------------------------------------------------+

Which user account is running Apache web server [www-data] ?
OK, Apache is running under user account www-data ;-)
```

```shell
+----------------------------------------------------------+
| Checking for Apache group...				   |
+----------------------------------------------------------+

Which user group is running Apache web server [www-data] ?
OK, Apache is running under users group www-data ;-)
```

```shell
+----------------------------------------------------------+
| Checking for Apache Include configuration directory...   |
+----------------------------------------------------------+

Setup found Apache Include configuration directory in
/etc/apache2/conf-available.
Setup will put OCS Inventory NG Apache configuration in this directory.
Where is Apache Include configuration directory [/etc/apache2/conf-available] ?
OK, Apache Include configuration directory /etc/apache2/conf-available found ;-)
```

## Perl

```shell
+----------------------------------------------------------+
| Checking for PERL Interpreter...			   |
+----------------------------------------------------------+

Found PERL Intrepreter at </usr/bin/perl> ;-)
Where is PERL Intrepreter binary [/usr/bin/perl] ?
OK, using PERL Intrepreter /usr/bin/perl ;-)
```

## Communication server

````shell
Do you wish to setup Communication server on this computer ([y]/n)?


+----------------------------------------------------------+
|             Checking for Make utility...                 |
+----------------------------------------------------------+

OK, Make utility found at </usr/bin/make> ;-)

+----------------------------------------------------------+
|        Checking for Apache mod_perl version...           |
+----------------------------------------------------------+

Checking for Apache mod_perl version 1.99_22 or higher
Found that mod_perl version 1.99_22 or higher is available.
OK, Apache is using mod_perl version 1.99_22 or higher ;-)
```

```shell
+----------------------------------------------------------+
|    Checking for Communication server log directory...    |
+----------------------------------------------------------+

Communication server can create detailed logs. This logs can be enabled
by setting integer value of LOGLEVEL to 1 in Administration console
menu Configuration.
Where to put Communication server log directory [/var/log/ocsinventory-server] ?
OK, Communication server will put logs into directory /var/log/ocsinventory-server ;-)
```

```shell
+----------------------------------------------------------------------------+
|    Checking for Communication server plugins configuration directory...    |
+----------------------------------------------------------------------------+

Communication server need a directory for plugins configuration files. 
Where to put Communication server plugins configuration files [/etc/ocsinventory-server/plugins] ?
OK, Communication server will put plugins configuration files into directory /etc/ocsinventory-server/plugins ;-)
```

```shell
-------------------------------------------------------------------+
|   Checking for Communication server plugins perl directory...     |
+-------------------------------------------------------------------+

Communication server need a directory for plugins Perl modules files.
Where to put Communication server plugins Perl modules files [/etc/ocsinventory-server/perl] ?
OK, Communication server will put plugins Perl modules files into directory /etc/ocsinventory-server/perl ;-)
```

## Perl modules


```shell
+----------------------------------------------------------+
| Checking for required Perl Modules...					|
+----------------------------------------------------------+

Checking for DBI PERL module...
Found that PERL module DBI is available.
Checking for Apache::DBI PERL module...
Found that PERL module Apache::DBI is available.
Checking for DBD::mysql PERL module...
Found that PERL module DBD::mysql is available.
Checking for Compress::Zlib PERL module...
Found that PERL module Compress::Zlib is available.
Checking for XML::Simple PERL module...
Found that PERL module XML::Simple is available.
Checking for Net::IP PERL module...
Found that PERL module Net::IP is available.
Checking for SOAP::Lite Perl module...
Found that PERL module SOAP::Lite is available.
Checking for Archive::Zip Perl module...
Found that PERL module Archive::Zip is available.
```

Ignore the next 2 warnings and just type ```ENTER```

```shell
+----------------------------------------------------------+
|         Checking for optional Perl Modules...            |
+----------------------------------------------------------+

Checking for Apache2::SOAP PERL module...
*** Warning: PERL module Apache2::SOAP is not installed !
This module is only required by OCS Inventory NG SOAP Web Service.
Do you wish to continue ([y]/n] ?
Checking for XML::Entities PERL module...
*** Warning: PERL module XML::Entities is not installed !
This module is only required by OCS Inventory NG SOAP Web Service.
Do you wish to continue ([y]/n] ?
```

The installer set some desired configurations:

```shell
+----------------------------------------------------------+
|                 OK, looks good ;-)                       |
|                                                          |
|     Configuring Communication server Perl modules...     |
+----------------------------------------------------------+

Checking if your kit is complete...
Looks good
Generating a Unix-style Makefile
Writing Makefile for Apache::Ocsinventory
Writing MYMETA.yml and MYMETA.json

+----------------------------------------------------------+
|                 OK, looks good ;-)                       |
|                                                          |
|      Preparing Communication server Perl modules...      |
+----------------------------------------------------------+


+----------------------------------------------------------+
|                 OK, prepare finshed ;-)                  |
|                                                          |
|     Installing Communication server Perl modules...      |
+----------------------------------------------------------+


+----------------------------------------------------------+
| OK, Communication server Perl modules install finished;-)|
|                                                          |
|     Creating Communication server log directory...       |
+----------------------------------------------------------+

Creating Communication server log directory /var/log/ocsinventory-server.

Fixing Communication server log directory files permissions.
Configuring logrotate for Communication server.
Removing old communication server logrotate file /etc/logrotate.d/ocsinventory-NG
Writing communication server logrotate to file /etc/logrotate.d/ocsinventory-server


+----------------------------------------------------------------------+
|        OK, Communication server log directory created ;-)            |
|                                                                      |
|   Creating Communication server plugins configuration directory...   |
+----------------------------------------------------------------------+

Creating Communication server plugins configuration directory /etc/ocsinventory-server/plugins.


+----------------------------------------------------------------------+
| OK, Communication server plugins configuration directory created ;-) |
|                                                                      |
|        Creating Communication server plugins Perl directory...       |
+----------------------------------------------------------------------+

Creating Communication server plugins Perl directory /etc/ocsinventory-server/perl.
```

```shell
+----------------------------------------------------------------------+
|     OK, Communication server plugins Perl directory created ;-)      |
|                                                                      |
|               Now configuring Apache web server...                   |
+----------------------------------------------------------------------+
+----------------------------------------------------------------------+
|     OK, Communication server plugins Perl directory created ;-)      |
|                                                                      |
|               Now configuring Apache web server...                   |
+----------------------------------------------------------------------+

To ensure Apache loads mod_perl before OCS Inventory NG Communication Server,
Setup can name Communication Server Apache configuration file
'z-ocsinventory-server.conf' instead of 'ocsinventory-server.conf'.
Do you allow Setup renaming Communication Server Apache configuration file
to 'z-ocsinventory-server.conf' ([y]/n) ?
OK, using 'z-ocsinventory-server.conf' as Communication Server Apache configuration file
Removing old communication server configuration to file /etc/apache2/conf-available/ocsinventory.conf
Writing communication server configuration to file /etc/apache2/conf-available/z-ocsinventory-server.conf

+----------------------------------------------------------------------+
|       OK, Communication server setup successfully finished ;-)       |
|                                                                      |
| Please, review /etc/apache2/conf-available/z-ocsinventory-server.conf |
|         to ensure all is good. Then restart Apache daemon.           |
+----------------------------------------------------------------------+
```

## Administration Server

```shell
Do you wish to setup Administration Server (Web Administration Console)
on this computer ([y]/n)?
```

```shell
+----------------------------------------------------------+
|    Checking for Administration Server directories...     |
+----------------------------------------------------------+

CAUTION: Setup now install files in accordance with Filesystem Hierarchy
Standard. So, no file is installed under Apache root document directory
(Refer to Apache configuration files to locate it).
If you're upgrading from OCS Inventory NG Server 1.01 and previous, YOU
MUST REMOVE (or move) directories 'ocsreports' and 'download' from Apache
root document directory.
If you choose to move directory, YOU MUST MOVE 'download' directory to
Administration Server writable/cache directory (by default
/var/lib/ocsinventory-reports), especially if you use deployment feature.

Do you wish to continue ([y]/n)?
Assuming directories 'ocsreports' and 'download' removed from
Apache root document directory.
```

```shell
Where to copy Administration Server static files for PHP Web Console
[/usr/share/ocsinventory-reports] ?
OK, using directory /usr/share/ocsinventory-reports to install static files ;-)
```

```shell
Where to create writable/cache directories for deployment packages,
administration console logs, IPDiscover and SNMP [/var/lib/ocsinventory-reports] ?
OK, writable/cache directory is /var/lib/ocsinventory-reports ;-)
```

Finally

```shell
+----------------------------------------------------------+
|         Checking for required Perl Modules...            |
+----------------------------------------------------------+

Checking for DBI PERL module...
Found that PERL module DBI is available.
Checking for DBD::mysql PERL module...
Found that PERL module DBD::mysql is available.
Checking for XML::Simple PERL module...
Found that PERL module XML::Simple is available.
Checking for Net::IP PERL module...
Found that PERL module Net::IP is available.

+----------------------------------------------------------+
|      Installing files for Administration server...       |
+----------------------------------------------------------+

Creating PHP directory /usr/share/ocsinventory-reports/ocsreports.
Copying PHP files to /usr/share/ocsinventory-reports/ocsreports.
Fixing permissions on directory /usr/share/ocsinventory-reports/ocsreports.
Creating database configuration file /usr/share/ocsinventory-reports/ocsreports/dbconfig.inc.php.
Creating IPDiscover directory /var/lib/ocsinventory-reports/ipd.
Fixing permissions on directory /var/lib/ocsinventory-reports/ipd.
Creating packages directory /var/lib/ocsinventory-reports/download.
Fixing permissions on directory /var/lib/ocsinventory-reports/download.
Creating snmp mibs directory /var/lib/ocsinventory-reports/snmp.
Fixing permissions on directory /var/lib/ocsinventory-reports/snmp.
Creating Administration server log files directory /var/lib/ocsinventory-reports/logs.
Fixing permissions on directory /var/lib/ocsinventory-reports/logs.
Creating Administration server scripts log files directory /var/lib/ocsinventory-reports/scripts.
Fixing permissions on directory /var/lib/ocsinventory-reports/scripts.
Configuring IPDISCOVER-UTIL Perl script.
Installing IPDISCOVER-UTIL Perl script.
Fixing permissions on IPDISCOVER-UTIL Perl script.
Writing Administration server configuration to file /etc/apache2/conf-available/ocsinventory-reports.conf

+----------------------------------------------------------------------+
|        OK, Administration server installation finished ;-)           |
|                                                                      |
| Please, review /etc/apache2/conf-available/ocsinventory-reports.conf
|          to ensure all is good and restart Apache daemon.            |
|                                                                      |
| Then, point your browser to http://server//ocsreports
|        to configure database server and create/update schema.        |
+----------------------------------------------------------------------+


Setup has created a log file /home/vagrant/OCSNG_UNIX_SERVER-2.3.1/ocs_server_setup.log. Please, save this file.
If you encounter error while running OCS Inventory NG Management server,
we can ask you to show us his content !

DON'T FORGET TO RESTART APACHE DAEMON !

Enjoy OCS Inventory NG ;-)
```

Restart apache

````shell
sudo service apache2 restart
```

