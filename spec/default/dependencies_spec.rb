require 'spec_helper'

packages = ['mysql-server',
            'libapache2-mod-php5',
            'libapache2-mod-perl2',
            'php5-mysql',                                                                                            
            'php5-gd',                                                                                               
            'php-pclzip',                                                                                            
            'libdbd-mysql-perl',
            'libxml-simple-perl',
            'libapache-dbi-perl',
            'libnet-ip-perl',
            'libsoap-lite-perl',
            'libarchive-zip-perl',
            'make']

describe 'Test dependencies' do
  packages.each do |pkg|
    describe package("#{pkg}") do
      it { should be_installed }
    end
  end
end
